package com.restrofront.testing.common;

public class Config {
    private static final String BROWSER = System.getProperty("browser", Constants.CHROME_BROWSER);
    private static final String DATA_SET = System.getProperty("dataSet", "QA");
    private static final String URL = System.getProperty("url", "https://www.ligonier.org/");
    private static final String IS_HEADLESS = System.getProperty("isHeadless","false");

    public static String getBrowser() {
        return BROWSER;
    }

    public static String getDataSet() {
        return DATA_SET;
    }

    public static String getUrl() {
        return URL;
    }

    public static Boolean isHeadless() {
        return Boolean.parseBoolean(IS_HEADLESS);
    }


}
