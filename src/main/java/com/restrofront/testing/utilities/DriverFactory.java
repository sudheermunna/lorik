package com.restrofront.testing.utilities;

import com.restrofront.testing.common.Config;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.concurrent.TimeUnit;

public class DriverFactory {

    private enum Browsers {
        IE,
        CHROME,
        NONE;

        /**
         * Creates browser name (IE, FIREFOX, CHROME, SAFARI)
         *
         * @return Browser instance
         */
        public static Browsers browserForName(String browser) throws IllegalArgumentException {
            for (Browsers b : values()) {
                if (b.toString().equalsIgnoreCase(browser)) {
                    return b;
                }
            }
            throw browserNotFound(browser);
        }

        private static IllegalArgumentException browserNotFound(String outcome) {
            return new IllegalArgumentException(("Invalid browser [" + outcome + "]"));
        }
    }

    /**
     * Returns current webDriver session based on browser name passed in
     *
     * @param browserInUse Browser name currently being used in the session
     * @return WebDriver instance
     */
    public static WebDriver getDriver(String browserInUse) {
        Browsers browser;
        WebDriver driver;

        if (browserInUse == null) {
            browser = Browsers.CHROME;
        } else {
            browser = Browsers.browserForName(browserInUse);
        }
        switch (browser) {
            case NONE:
                return null;
            case CHROME:
                driver = createChromeDriver();
                break;
            case IE:
            default:
               throw new WebDriverException("No Browser Configured");
        }
        addAllBrowserSetup(driver);
        return driver;
    }

    /**
     * Creates and returns instance of Chrome driver with desired capabilities
     *
     * @return Chrome driver
     */
    private static WebDriver createChromeDriver() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
//        if(Config.isHeadless())
//           options.addArguments("--headless");
        return new ChromeDriver(options);
    }

    /**
     * Sets implicit wait and page load timeout for current session
     * Also maximizes the browser window
     *
     * @param driver Current instance of webDriver
     */
    private static void addAllBrowserSetup(WebDriver driver) {
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
            driver.manage().window().maximize();
    }
}
