package com.restrofront.testing.utilities;

import java.util.concurrent.ConcurrentHashMap;

public class ScenarioData {

    private ConcurrentHashMap<String, String> genericData = new ConcurrentHashMap<>();

    public void setData(String key, String value) {
        genericData.put(key,value);
    }

    public String getData(String key) {
        return genericData.get(key);
    }

}