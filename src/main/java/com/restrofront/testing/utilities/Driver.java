package com.restrofront.testing.utilities;

import com.restrofront.testing.common.Config;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.logging.Logger;

public class Driver {

    private WebDriver driver;
    private static final String BROWSER = "CHROME";
    private final Logger LOGGER = Logger.getLogger(Driver.class.getName());
    public ScenarioData scenarioData = new ScenarioData();
    Actions actions;

    public Driver(WebDriver driver){
        this.driver = driver;
    }

    public void initialize() {
        if (driver == null)
            createNewDriverInstance();
    }

    private static String ELEMENT_REPOSITORY_PATH = System.getProperty("user.dir") + "\\src\\test\\resources\\ElementsRepository\\";

    /**
     * Creates a new driver instance
     */
    private void createNewDriverInstance() {
        driver = DriverFactory.getDriver(BROWSER);
        actions = new Actions(driver);
    }

    /**
     * Returns the current driver
     *
     * @return driver
     */
    public WebDriver getDriver() {
        return driver;
    }

    /**
     * Quits the current driver session
     */
    public void destroyDriver() {
        LOGGER.info("destroyDriver started");
        try {
            driver.quit();
            driver = null;
        } catch (Exception e) {
            LOGGER.info("Exception: " + e.getMessage());
        }
        LOGGER.info("destroyDriver completed");
    }

    public void get(String url) {
        driver.get(url);
        driver.manage().window().maximize();
    }

    private void sendKeys(WebElement element, String input) {
        waitForElementClickable(element);
        element.click();
        element.clear();
        element.sendKeys(input);
    }

    public WebElement returnElementContainsText(String text) {
        return driver.findElement(By.xpath("//*[contains(text(),'" + text + "')]"));
    }

    public WebElement returnElementContainsText(WebDriver driver, String text, int position) {
        return driver.findElement(By.xpath("//*[contains(text(),'" + text + "')]['" + position + "']"));
    }

    public WebElement returnElementContainsText(String text, String tagName) {
        return driver.findElement(By.xpath("//" + tagName + "[contains(text(),'" + text + "')]"));
    }

    public WebElement returnElementContainsText(String text, String tagName, String position) {
        return driver.findElement(By.xpath("//" + tagName + "[contains(text(),'" + text + "')][" + position + "]"));
    }

    public void clickOnElementWithText(String text) {
        returnElementContainsText(text).click();
    }

    public void actionClickOnElementWithText(String text) {
        actions.click(returnElementContainsText(text)).perform();
    }

//    public void clickOnElementWithText(String text, int position) {
//        returnElementContainsText(text, position).click();
//    }
//
//    public void actionClickOnElementWithText(String text, int position) {
//        try {
//            actions.click(returnElementContainsText(text, position)).perform();
//        } catch (JavascriptException js) {
//            jsClick(returnElementContainsText(text, position));
//        }
//    }

    public void jsClick(WebElement element) {
        String mouseOverScript = "arguments[0].click();";
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        javascriptExecutor.executeScript(mouseOverScript, element);
    }

    public void clickOnElementWithText(String text, String tagName) {
        try {
            returnElementContainsText(text, tagName).click();
        } catch (ElementNotInteractableException en) {
            actions.click(returnElementContainsText(text, tagName)).perform();
        }
    }

    public void actionClickOnElementWithText(String text, String tagName) {
        actions.click(returnElementContainsText(text, tagName)).perform();
    }

    public void clickOnElementWithText(String text, String tagName, String position) {
        returnElementContainsText(text, tagName, position).click();
    }

    public void waitForElementClickable(WebElement element, int timeInSeconds) {
        WebDriverWait webDriverWait = new WebDriverWait(driver, timeInSeconds);
        try {
            webDriverWait.until(ExpectedConditions.elementToBeClickable(element));
        } catch (StaleElementReferenceException s) {
//            WebElement staleElement = driver.findElement(extractByLocatorFromWebElement(element));
//            webDriverWait.until(ExpectedConditions.elementToBeClickable(staleElement));
            throw new StaleElementReferenceException("s");
        }
    }

    public void waitForElementToBeVisible(WebElement element, int timeInSeconds) {
        WebDriverWait webDriverWait = new WebDriverWait(driver, timeInSeconds);
        try {
            webDriverWait.until(ExpectedConditions.visibilityOf(element));
        } catch (StaleElementReferenceException s) {
            WebElement staleElement = driver.findElement(extractByLocatorFromWebElement(element));
            webDriverWait.until(ExpectedConditions.visibilityOf(staleElement));
        }
    }

    public By extractByLocatorFromWebElement(WebElement element) {
        String elementContent = element.toString();
        String unCleanXpath = elementContent.substring(elementContent.indexOf("xpath:") + 6);
        String cleanXpath = unCleanXpath.substring(0, unCleanXpath.length() - 1);
        return By.xpath(cleanXpath);
    }

    public void waitForMilliSeconds(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            LOGGER.info(e.toString());
        }
    }

    public void waitForElementClickable(WebElement element) {
        waitForElementClickable(element, 30);
    }

    public void waitForElementToBeVisible(WebElement element) {
        waitForElementToBeVisible(element, 30);
    }

    public void saveTextAttribute(WebElement element, String key) {
        waitForElementToBeVisible(element);
        String text = element.getText().trim();
        scenarioData.setData(key, text);
        LOGGER.info("Text attribute of the element is: " + text);
    }

    public void saveValueAttribute(WebElement element, String key) {
        waitForElementToBeVisible(element);
        String value = element.getAttribute("value").trim();
        scenarioData.setData(key, value);
        LOGGER.info("Value attribute of the element is: " + value);
    }

    public void saveTitleAttribute(WebElement element, String key) {
        waitForElementToBeVisible(element);
        String title = element.getAttribute("title").trim();
        scenarioData.setData(key, title);
        LOGGER.info("Title attribute of the element is: " + title);
    }

}
