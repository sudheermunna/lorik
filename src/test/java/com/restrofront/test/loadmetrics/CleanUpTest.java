package com.restrofront.test.loadmetrics;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;

public class CleanUpTest {

    @Test
    public void cleanUpTask() throws IOException {
        File file = new File("C:\\windows\\Temp");
        String[] directories = file.list(new FilenameFilter() {
            @Override
            public boolean accept(File current, String name) {
                return new File(current, name).isDirectory();
            }
        });
       if(directories != null) {
           for (String directory : directories) {
               System.out.println(directory);
                if(directory.contains("scoped")){
                    FileUtils.deleteDirectory(new File("C:\\windows\\Temp\\"+directory));
                }
           }
       }
        System.out.println(Arrays.toString(directories));
    }
}
