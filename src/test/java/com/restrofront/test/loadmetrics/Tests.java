package com.restrofront.test.loadmetrics;

import com.restrofront.testing.utilities.Driver;
import com.restrofront.testing.utilities.DriverFactory;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.openqa.selenium.*;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Execution(ExecutionMode.CONCURRENT)
public class Tests {

    private void openClientWebsite(int autouserNumber) {
        String url = "https://autouser1.cstg.restrofront.com/";
//        String url = "http://localhost:3000";
        WebDriver webDriver = DriverFactory.getDriver("CHROME");
        try {
            webDriver.get(url);
            Driver driver1 = new Driver(webDriver);
            webDriver.manage().window().setSize(new Dimension(1200, 800));
            WebElement loginButton = webDriver.findElement(By.xpath("(//*[text()='Log In'])[1]"));
            driver1.waitForElementClickable(loginButton);
            loginButton.click();
            WebElement phoneNumber = webDriver.findElement(By.xpath("//label[text()='Phone Number']/preceding::input"));
            phoneNumber.click();
            phoneNumber.clear();
            phoneNumber.sendKeys("1000000000");
            driver1.clickOnElementWithText("Next");
            WebElement password = webDriver.findElement(By.xpath("//label[text()='Password']/preceding::input"));
            password.click();
            password.clear();
            password.sendKeys("aaaaaaaa");
            WebElement login = webDriver.findElement(By.xpath("//button[text()='Login']"));
            driver1.waitForElementClickable(login);
            login.click();
            driver1.waitForMilliSeconds(5000);
            driver1.clickOnElementWithText("Locations");
            WebElement element = webDriver.findElement(By.xpath("//div[@class='location-view']"));
            driver1.waitForElementClickable(element);
            element.click();
            driver1.waitForMilliSeconds(5000);
//            List<WebElement> elements = webDriver.findElements(By.xpath("//div[@class='subcat-header']"));
//            for (int i=0;i<elements.size();i++){
//                driver1.waitForMilliSeconds(1000);
//                WebElement element1 = webDriver.findElement(By.xpath("(//div[@class='subcat-header'])["+(i+1)+"]"));
//                driver1.waitForElementClickable(element1);
//                element1.click();
//                // Logic to validate
//            }

            System.out.println("Test Completed Succesfully!");
//        try {
//            Thread.sleep(5000);
//        }catch (Exception e){
//            e.printStackTrace();
//        }
        }catch (Exception e){
            takeScreenshot(webDriver);
            e.printStackTrace();
        }   finally {

            webDriver.quit();
        }

    }

    private void takeScreenshot(WebDriver webDriver) {
        TakesScreenshot scrShot =((TakesScreenshot)webDriver);
        File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);

        //Move image file to new destination


        File DestFile=new File("C:\\Jenkins\\screenshots\\"+System.currentTimeMillis() / 1000L+".png");

        //Copy file at destination

        try{
            FileUtils.copyFile(SrcFile, DestFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void test1(){
        openClientWebsite(1);
    }

    @Test
    public void test2(){
        openClientWebsite(2);
    }

    @Test
    public void test3(){
        openClientWebsite(3);
    }

    @Test
    public void test4(){
        openClientWebsite(4);
    }

    @Test
    public void test5(){
        openClientWebsite(5);
    }

    @Test
    public void test6(){
        openClientWebsite(6);
    }

    @Test
    public void test7(){
        openClientWebsite(7);
    }

    @Test
    public void test8(){
        openClientWebsite(8);
    }

    @Test
    public void test9(){
        openClientWebsite(9);
    }

    @Test
    public void test10(){
        openClientWebsite(10);
    }
    @Test
    public void test11(){
        openClientWebsite(11);
    }
    @Test
    public void test12(){
        openClientWebsite(12);
    }

    @Test
    public void test13(){
        openClientWebsite(13);
    }

    @Test
    public void test14(){
        openClientWebsite(14);
    }

    @Test
    public void test15(){
        openClientWebsite(15);
    }

    @Test
    public void test16(){
        openClientWebsite(16);
    }

    @Test
    public void test17(){
        openClientWebsite(17);
    }

    @Test
    public void test18(){
        openClientWebsite(18);
    }

    @Test
    public void test19(){
        openClientWebsite(19);
    }

    @Test
    public void test20(){
        openClientWebsite(20);
    }

    @Test
    public void test21(){
        openClientWebsite(21);
    }

    @Test
    public void test22(){
        openClientWebsite(22);
    }

    @Test
    public void test23(){
        openClientWebsite(23);
    }
    @Test
    public void test24(){
        openClientWebsite(24);
    }
    @Test
    public void test25(){
        openClientWebsite(25);
    }

    @Test
    public void test26(){
        openClientWebsite(26);
    }

    @Test
    public void test27(){
        openClientWebsite(27);
    }

    @Test
    public void test28(){
        openClientWebsite(28);
    }

    @Test
    public void test29(){
        openClientWebsite(29);
    }

    @Test
    public void test30(){
        openClientWebsite(30);
    }

    @Test
    public void test31(){
        openClientWebsite(31);
    }

    @Test
    public void test32(){
        openClientWebsite(32);
    }

    @Test
    public void test33(){
        openClientWebsite(33);
    }

    @Test
    public void test34(){
        openClientWebsite(34);
    }

    @Test
    public void test35(){
        openClientWebsite(35);
    }

    @Test
    public void test36(){
        openClientWebsite(36);
    }
    @Test
    public void test37(){
        openClientWebsite(37);
    }
    @Test
    public void test38(){
        openClientWebsite(38);
    }

    @Test
    public void test39(){
        openClientWebsite(39);
    }

    @Test
    public void test40(){
        openClientWebsite(40);
    }

    @Test
    public void test41(){
        openClientWebsite(41);
    }

    @Test
    public void test42(){
        openClientWebsite(42);
    }

    @Test
    public void test43(){
        openClientWebsite(43);
    }

    @Test
    public void test44(){
        openClientWebsite(44);
    }

    @Test
    public void test45(){
        openClientWebsite(45);
    }

    @Test
    public void test46(){
        openClientWebsite(46);
    }

    @Test
    public void test47(){
        openClientWebsite(47);
    }

    @Test
    public void test48(){
        openClientWebsite(48);
    }

    @Test
    public void test49(){
        openClientWebsite(49);
    }
    @Test
    public void test50(){
        openClientWebsite(50);
    }
    @Test
    public void test51(){
        openClientWebsite(51);
    }

    @Test
    public void test52(){
        openClientWebsite(52);
    }

    @Test
    public void test53(){
        openClientWebsite(53);
    }

    @Test
    public void test54(){
        openClientWebsite(54);
    }

    @Test
    public void test55(){
        openClientWebsite(55);
    }

    @Test
    public void test56(){
        openClientWebsite(56);
    }

    @Test
    public void test57(){
        openClientWebsite(57);
    }

    @Test
    public void test58(){
        openClientWebsite(58);
    }

    @Test
    public void test59(){
        openClientWebsite(59);
    }

    @Test
    public void test60(){
        openClientWebsite(60);
    }

    @Test
    public void test61(){
        openClientWebsite(61);
    }

    @Test
    public void test62(){
        openClientWebsite(62);
    }
    @Test
    public void test63(){
        openClientWebsite(63);
    }
    @Test
    public void test64(){
        openClientWebsite(64);
    }

    @Test
    public void test65(){
        openClientWebsite(65);
    }

    @Test
    public void test66(){
        openClientWebsite(66);
    }

    @Test
    public void test67(){
        openClientWebsite(67);
    }

    @Test
    public void test68(){
        openClientWebsite(68);
    }

    @Test
    public void test69(){
        openClientWebsite(69);
    }

    @Test
    public void test70(){
        openClientWebsite(70);
    }

    @Test
    public void test71(){
        openClientWebsite(71);
    }

    @Test
    public void test72(){
        openClientWebsite(72);
    }

    @Test
    public void test73(){
        openClientWebsite(73);
    }

    @Test
    public void test74(){
        openClientWebsite(74);
    }

    @Test
    public void test75(){
        openClientWebsite(75);
    }

    @Test
    public void test76(){
        openClientWebsite(76);
    }

    @Test
    public void test77(){
        openClientWebsite(77);
    }

    @Test
    public void test78(){
        openClientWebsite(78);
    }

    @Test
    public void test79(){
        openClientWebsite(79);
    }

    @Test
    public void test80(){
        openClientWebsite(80);
    }

    @Test
    public void test81(){
        openClientWebsite(81);
    }

    @Test
    public void test82(){
        openClientWebsite(82);
    }

    @Test
    public void test83(){
        openClientWebsite(83);
    }

    @Test
    public void test84(){
        openClientWebsite(84);
    }

    @Test
    public void test85(){
        openClientWebsite(85);
    }

    @Test
    public void test86(){
        openClientWebsite(86);
    }

    @Test
    public void test87(){
        openClientWebsite(87);
    }

    @Test
    public void test88(){
        openClientWebsite(88);
    }

    @Test
    public void test89(){
        openClientWebsite(89);
    }

    @Test
    public void test90(){
        openClientWebsite(90);
    }

    @Test
    public void test91(){
        openClientWebsite(91);
    }

    @Test
    public void test92(){
        openClientWebsite(92);
    }

    @Test
    public void test93(){
        openClientWebsite(93);
    }

    @Test
    public void test94(){
        openClientWebsite(94);
    }

    @Test
    public void test95(){
        openClientWebsite(95);
    }

    @Test
    public void test96(){
        openClientWebsite(96);
    }

    @Test
    public void test97(){
        openClientWebsite(97);
    }

    @Test
    public void test98(){
        openClientWebsite(98);
    }

    @Test
    public void test99(){
        openClientWebsite(99);
    }

    @Test
    public void test100(){
        openClientWebsite(100);
    }
}
