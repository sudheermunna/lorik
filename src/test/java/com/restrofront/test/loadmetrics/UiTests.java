package com.restrofront.test.loadmetrics;

import com.restrofront.testing.utilities.Driver;
import com.restrofront.testing.utilities.DriverFactory;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Execution(ExecutionMode.CONCURRENT)
public class UiTests {

    private void openClientWebsite(int autouserNumber) {
        String url = "https://autouser1.cstg.restrofront.com/";
//        String url = "http://localhost:3000";
        WebDriver webDriver = DriverFactory.getDriver("CHROME");
        try{
            webDriver.get(url);
            Driver driver1 = new Driver(webDriver);
            webDriver.manage().window().setSize(new Dimension(1200, 800));
            WebElement loginButton = webDriver.findElement(By.xpath("(//*[text()='Log In'])[1]"));
            driver1.waitForElementClickable(loginButton);
            loginButton.click();
            WebElement phoneNumber = webDriver.findElement(By.xpath("//label[text()='Phone Number']/preceding::input"));
            phoneNumber.click();
            phoneNumber.clear();
            phoneNumber.sendKeys("1000000000");
            driver1.clickOnElementWithText("Next");
            WebElement password = webDriver.findElement(By.xpath("//label[text()='Password']/preceding::input"));
            password.click();
            password.clear();
            password.sendKeys("aaaaaaaa");
            WebElement login = webDriver.findElement(By.xpath("//button[text()='Login']"));
            driver1.waitForElementClickable(login);
            login.click();
            driver1.waitForMilliSeconds(5000);
            driver1.clickOnElementWithText("Locations");
            WebElement element = webDriver.findElement(By.xpath("//div[@class='location-view']"));
            driver1.waitForElementClickable(element);
            element.click();
            driver1.waitForMilliSeconds(5000);
//            List<WebElement> elements = webDriver.findElements(By.xpath("//div[@class='subcat-header']"));
//            for (int i=0;i<elements.size();i++){
//                driver1.waitForMilliSeconds(1000);
//                WebElement element1 = webDriver.findElement(By.xpath("(//div[@class='subcat-header'])["+(i+1)+"]"));
//                driver1.waitForElementClickable(element1);
//                element1.click();
//                // Logic to validate
//            }
            System.out.println("Test Completed Succesfully!");
//        try {
//            Thread.sleep(5000);
//        }catch (Exception e){
//            e.printStackTrace();
//        }
        } finally {
            webDriver.quit();
        }
    }

    @Test
    public void test1() throws InterruptedException {
//            openClientWebsite(1);
        CompletableFuture<Void> runTest1 = CompletableFuture.runAsync(() -> openClientWebsite(1));
        CompletableFuture<Void> runTest2 = CompletableFuture.runAsync(() -> openClientWebsite(2));
        CompletableFuture<Void> runTest3 = CompletableFuture.runAsync(() -> openClientWebsite(3));
        CompletableFuture<Void> runTest4 = CompletableFuture.runAsync(() -> openClientWebsite(4));
        CompletableFuture<Void> runTest5 = CompletableFuture.runAsync(() -> openClientWebsite(5));
        CompletableFuture<Void> runTest6 = CompletableFuture.runAsync(() -> openClientWebsite(6));
        CompletableFuture<Void> runTest7 = CompletableFuture.runAsync(() -> openClientWebsite(7));
        CompletableFuture<Void> runTest8 = CompletableFuture.runAsync(() -> openClientWebsite(8));
        CompletableFuture<Void> runTest9 = CompletableFuture.runAsync(() -> openClientWebsite(9));
        CompletableFuture<Void> runTest10 = CompletableFuture.runAsync(() -> openClientWebsite(10));

        CompletableFuture.allOf(runTest1,runTest2,runTest3,runTest4,runTest5,runTest6,runTest7,runTest8,runTest9,runTest10).join();
    }

    @Test
    public void test2() throws InterruptedException {
//            openClientWebsite(1);
        CompletableFuture<Void> runTest1 = CompletableFuture.runAsync(() -> openClientWebsite(11));
        CompletableFuture<Void> runTest2 = CompletableFuture.runAsync(() -> openClientWebsite(12));
        CompletableFuture<Void> runTest3 = CompletableFuture.runAsync(() -> openClientWebsite(13));
        CompletableFuture<Void> runTest4 = CompletableFuture.runAsync(() -> openClientWebsite(14));
        CompletableFuture<Void> runTest5 = CompletableFuture.runAsync(() -> openClientWebsite(15));
        CompletableFuture<Void> runTest6 = CompletableFuture.runAsync(() -> openClientWebsite(16));
        CompletableFuture<Void> runTest7 = CompletableFuture.runAsync(() -> openClientWebsite(17));
        CompletableFuture<Void> runTest8 = CompletableFuture.runAsync(() -> openClientWebsite(18));
        CompletableFuture<Void> runTest9 = CompletableFuture.runAsync(() -> openClientWebsite(19));
        CompletableFuture<Void> runTest10 = CompletableFuture.runAsync(() -> openClientWebsite(20));

        CompletableFuture.allOf(runTest1,runTest2,runTest3,runTest4,runTest5,runTest6,runTest7,runTest8,runTest9,runTest10).join();
    }

    @Test
    public void test3() throws InterruptedException {
//            openClientWebsite(1);
        CompletableFuture<Void> runTest1 = CompletableFuture.runAsync(() -> openClientWebsite(21));
        CompletableFuture<Void> runTest2 = CompletableFuture.runAsync(() -> openClientWebsite(22));
        CompletableFuture<Void> runTest3 = CompletableFuture.runAsync(() -> openClientWebsite(23));
        CompletableFuture<Void> runTest4 = CompletableFuture.runAsync(() -> openClientWebsite(24));
        CompletableFuture<Void> runTest5 = CompletableFuture.runAsync(() -> openClientWebsite(25));
        CompletableFuture<Void> runTest6 = CompletableFuture.runAsync(() -> openClientWebsite(26));
        CompletableFuture<Void> runTest7 = CompletableFuture.runAsync(() -> openClientWebsite(27));
        CompletableFuture<Void> runTest8 = CompletableFuture.runAsync(() -> openClientWebsite(28));
        CompletableFuture<Void> runTest9 = CompletableFuture.runAsync(() -> openClientWebsite(29));
        CompletableFuture<Void> runTest10 = CompletableFuture.runAsync(() -> openClientWebsite(30));

        CompletableFuture.allOf(runTest1,runTest2,runTest3,runTest4,runTest5,runTest6,runTest7,runTest8,runTest9,runTest10).join();
    }

    @Test
    public void test4() throws InterruptedException {
//            openClientWebsite(1);
        CompletableFuture<Void> runTest1 = CompletableFuture.runAsync(() -> openClientWebsite(31));
        CompletableFuture<Void> runTest2 = CompletableFuture.runAsync(() -> openClientWebsite(32));
        CompletableFuture<Void> runTest3 = CompletableFuture.runAsync(() -> openClientWebsite(33));
        CompletableFuture<Void> runTest4 = CompletableFuture.runAsync(() -> openClientWebsite(34));
        CompletableFuture<Void> runTest5 = CompletableFuture.runAsync(() -> openClientWebsite(35));
        CompletableFuture<Void> runTest6 = CompletableFuture.runAsync(() -> openClientWebsite(36));
        CompletableFuture<Void> runTest7 = CompletableFuture.runAsync(() -> openClientWebsite(37));
        CompletableFuture<Void> runTest8 = CompletableFuture.runAsync(() -> openClientWebsite(38));
        CompletableFuture<Void> runTest9 = CompletableFuture.runAsync(() -> openClientWebsite(39));
        CompletableFuture<Void> runTest10 = CompletableFuture.runAsync(() -> openClientWebsite(40));

        CompletableFuture.allOf(runTest1,runTest2,runTest3,runTest4,runTest5,runTest6,runTest7,runTest8,runTest9,runTest10).join();
    }

    @Test
    public void test5() throws InterruptedException {
//            openClientWebsite(1);
        CompletableFuture<Void> runTest1 = CompletableFuture.runAsync(() -> openClientWebsite(41));
        CompletableFuture<Void> runTest2 = CompletableFuture.runAsync(() -> openClientWebsite(42));
        CompletableFuture<Void> runTest3 = CompletableFuture.runAsync(() -> openClientWebsite(43));
        CompletableFuture<Void> runTest4 = CompletableFuture.runAsync(() -> openClientWebsite(44));
        CompletableFuture<Void> runTest5 = CompletableFuture.runAsync(() -> openClientWebsite(45));
        CompletableFuture<Void> runTest6 = CompletableFuture.runAsync(() -> openClientWebsite(46));
        CompletableFuture<Void> runTest7 = CompletableFuture.runAsync(() -> openClientWebsite(47));
        CompletableFuture<Void> runTest8 = CompletableFuture.runAsync(() -> openClientWebsite(48));
        CompletableFuture<Void> runTest9 = CompletableFuture.runAsync(() -> openClientWebsite(49));
        CompletableFuture<Void> runTest10 = CompletableFuture.runAsync(() -> openClientWebsite(50));

        CompletableFuture.allOf(runTest1,runTest2,runTest3,runTest4,runTest5,runTest6,runTest7,runTest8,runTest9,runTest10).join();
    }

    @Test
    public void test6() throws InterruptedException {
//            openClientWebsite(1);
        CompletableFuture<Void> runTest1 = CompletableFuture.runAsync(() -> openClientWebsite(51));
        CompletableFuture<Void> runTest2 = CompletableFuture.runAsync(() -> openClientWebsite(52));
        CompletableFuture<Void> runTest3 = CompletableFuture.runAsync(() -> openClientWebsite(53));
        CompletableFuture<Void> runTest4 = CompletableFuture.runAsync(() -> openClientWebsite(54));
        CompletableFuture<Void> runTest5 = CompletableFuture.runAsync(() -> openClientWebsite(55));
        CompletableFuture<Void> runTest6 = CompletableFuture.runAsync(() -> openClientWebsite(56));
        CompletableFuture<Void> runTest7 = CompletableFuture.runAsync(() -> openClientWebsite(57));
        CompletableFuture<Void> runTest8 = CompletableFuture.runAsync(() -> openClientWebsite(58));
        CompletableFuture<Void> runTest9 = CompletableFuture.runAsync(() -> openClientWebsite(59));
        CompletableFuture<Void> runTest10 = CompletableFuture.runAsync(() -> openClientWebsite(60));

        CompletableFuture.allOf(runTest1,runTest2,runTest3,runTest4,runTest5,runTest6,runTest7,runTest8,runTest9,runTest10).join();
    }

    @Test
    public void test7() throws InterruptedException {
//            openClientWebsite(1);
        CompletableFuture<Void> runTest1 = CompletableFuture.runAsync(() -> openClientWebsite(61));
        CompletableFuture<Void> runTest2 = CompletableFuture.runAsync(() -> openClientWebsite(62));
        CompletableFuture<Void> runTest3 = CompletableFuture.runAsync(() -> openClientWebsite(63));
        CompletableFuture<Void> runTest4 = CompletableFuture.runAsync(() -> openClientWebsite(64));
        CompletableFuture<Void> runTest5 = CompletableFuture.runAsync(() -> openClientWebsite(65));
        CompletableFuture<Void> runTest6 = CompletableFuture.runAsync(() -> openClientWebsite(66));
        CompletableFuture<Void> runTest7 = CompletableFuture.runAsync(() -> openClientWebsite(67));
        CompletableFuture<Void> runTest8 = CompletableFuture.runAsync(() -> openClientWebsite(68));
        CompletableFuture<Void> runTest9 = CompletableFuture.runAsync(() -> openClientWebsite(69));
        CompletableFuture<Void> runTest10 = CompletableFuture.runAsync(() -> openClientWebsite(70));

        CompletableFuture.allOf(runTest1,runTest2,runTest3,runTest4,runTest5,runTest6,runTest7,runTest8,runTest9,runTest10).join();
    }

    @Test
    public void test8() throws InterruptedException {
//            openClientWebsite(1);
        CompletableFuture<Void> runTest1 = CompletableFuture.runAsync(() -> openClientWebsite(71));
        CompletableFuture<Void> runTest2 = CompletableFuture.runAsync(() -> openClientWebsite(72));
        CompletableFuture<Void> runTest3 = CompletableFuture.runAsync(() -> openClientWebsite(73));
        CompletableFuture<Void> runTest4 = CompletableFuture.runAsync(() -> openClientWebsite(74));
        CompletableFuture<Void> runTest5 = CompletableFuture.runAsync(() -> openClientWebsite(75));
        CompletableFuture<Void> runTest6 = CompletableFuture.runAsync(() -> openClientWebsite(76));
        CompletableFuture<Void> runTest7 = CompletableFuture.runAsync(() -> openClientWebsite(77));
        CompletableFuture<Void> runTest8 = CompletableFuture.runAsync(() -> openClientWebsite(78));
        CompletableFuture<Void> runTest9 = CompletableFuture.runAsync(() -> openClientWebsite(79));
        CompletableFuture<Void> runTest10 = CompletableFuture.runAsync(() -> openClientWebsite(80));

        CompletableFuture.allOf(runTest1,runTest2,runTest3,runTest4,runTest5,runTest6,runTest7,runTest8,runTest9,runTest10).join();
    }

    @Test
    public void test9() throws InterruptedException {
//            openClientWebsite(1);
        CompletableFuture<Void> runTest1 = CompletableFuture.runAsync(() -> openClientWebsite(81));
        CompletableFuture<Void> runTest2 = CompletableFuture.runAsync(() -> openClientWebsite(82));
        CompletableFuture<Void> runTest3 = CompletableFuture.runAsync(() -> openClientWebsite(83));
        CompletableFuture<Void> runTest4 = CompletableFuture.runAsync(() -> openClientWebsite(84));
        CompletableFuture<Void> runTest5 = CompletableFuture.runAsync(() -> openClientWebsite(85));
        CompletableFuture<Void> runTest6 = CompletableFuture.runAsync(() -> openClientWebsite(86));
        CompletableFuture<Void> runTest7 = CompletableFuture.runAsync(() -> openClientWebsite(87));
        CompletableFuture<Void> runTest8 = CompletableFuture.runAsync(() -> openClientWebsite(88));
        CompletableFuture<Void> runTest9 = CompletableFuture.runAsync(() -> openClientWebsite(89));
        CompletableFuture<Void> runTest10 = CompletableFuture.runAsync(() -> openClientWebsite(90));

        CompletableFuture.allOf(runTest1,runTest2,runTest3,runTest4,runTest5,runTest6,runTest7,runTest8,runTest9,runTest10).join();
    }

    @Test
    public void test10() throws InterruptedException {
//            openClientWebsite(1);
        CompletableFuture<Void> runTest1 = CompletableFuture.runAsync(() -> openClientWebsite(91));
        CompletableFuture<Void> runTest2 = CompletableFuture.runAsync(() -> openClientWebsite(92));
        CompletableFuture<Void> runTest3 = CompletableFuture.runAsync(() -> openClientWebsite(93));
        CompletableFuture<Void> runTest4 = CompletableFuture.runAsync(() -> openClientWebsite(94));
        CompletableFuture<Void> runTest5 = CompletableFuture.runAsync(() -> openClientWebsite(95));
        CompletableFuture<Void> runTest6 = CompletableFuture.runAsync(() -> openClientWebsite(96));
        CompletableFuture<Void> runTest7 = CompletableFuture.runAsync(() -> openClientWebsite(97));
        CompletableFuture<Void> runTest8 = CompletableFuture.runAsync(() -> openClientWebsite(98));
        CompletableFuture<Void> runTest9 = CompletableFuture.runAsync(() -> openClientWebsite(99));
        CompletableFuture<Void> runTest10 = CompletableFuture.runAsync(() -> openClientWebsite(100));

        CompletableFuture.allOf(runTest1,runTest2,runTest3,runTest4,runTest5,runTest6,runTest7,runTest8,runTest9,runTest10).join();
    }
}
